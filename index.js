// initialization
const express = require("express");
const app = express();
const port = 3000;

// mock database
let users = [
  {"username": 'Johndoe',
  "password": 'johndoe1234' }
];

// application express
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// routes
app.get("/home", (request, response) => {
	response.send("Welcome to the homepage!");

});

app.get("/users", (request, response) => {
	response.json(users);

});

app.delete('/delete-user', (request, response) => {
  let message;
  let userIndex = -1;

  // Find the index of the user in the database
  for(let i = 0; i < users.length; i++) {
    if(request.body.username == users[i].username){
      userIndex = i;
      break;
    }
  }

  // If the user was found, remove them from the database
  if (userIndex !== -1) {
    users.splice(userIndex, 1);
    message = `${request.body.username} has been deleted.`;
  } else {
    message = "User does not exist.";
  }

  response.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));